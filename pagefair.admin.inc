<?php

/**
 * @file
 * Administrative page callbacks for the pagefair module.
 */

/**
 * Implements hook_admin_settings() for configuring the module.
 *
 * @todo: Add ability to create blocks with Pagefair ad IDs
 */
function pagefair_admin_settings_form($form_state) {

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );


  $form['general']['pagefair_website_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Pagefair website code'),
    '#default_value' => variable_get('pagefair_website_code', ''),
    '#size' => 18,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('This code is unique per site.  The code can be found from within your Pagefair account.'),
  );

  return system_settings_form($form);
}
