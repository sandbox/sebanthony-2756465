/**
 * @file
 * Javascript that adds the pagefair analytics and ad serving libraries
 * to the page.
 */

(function ($) {

    function async_load(script_url){
        var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://');
        var s = document.createElement('script'); s.src = protocol + script_url;
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    }

    $(document).ready(function(){
        bm_website_code = Drupal.settings.pagefair.pagefairWebsiteCode;
        async_load('asset.pagefair.com/measure.min.js')
    });
    $(document).ready(function(){
        bm_website_code = Drupal.settings.pagefair.pagefairWebsiteCode;
        async_load('asset.pagefair.net/ads.min.js')
    });

})(jQuery);
